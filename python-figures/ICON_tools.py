import pandas as pd
import xarray as xr
import numpy as np
from os import path
import cftime

def convert_climatetime(timein):
    """
    Converts the time variable of the ICON climatology dataset to a datetime object.
    The time variable is in the format YYYYMMDDHHMMSS. The function converts it to a datetime object.
    
    Parameters
    ----------
    timein : array_like
        time variable of the ICON climatology dataset
    
    Returns
    ----------
    time : array_like
        datetime object
    """
    date=pd.to_datetime(timein,format="%Y%m%d")
    hours=((timein%1)*24).round(6)
    time=date+pd.to_timedelta(hours,unit="hours")
    return time


def load_experiment(expname): #loads the dataset of a simulation
    """
    Loads the dataset of a simulation.

    Parameters
    ----------
    expname : string
        name of the experiment 

    Returns
    ----------
    expname : string
        name of the experiment 
    DS : xarray dataset
        dataset of the simulation
    """

    fname = expname +"_atm_2d_ml.ym.gm.nc" #filename of global yearly mean
    dpath = "/work/bb1092/pp_JH/" +expname +"/" #simulation path
    DS = xr.open_dataset(dpath +fname, decode_times=False) #loading of dataset
    print(dpath +fname)
    return expname, DS # returns the name of the experiment & the actual dataset


def get_explist(data_path, explist):
    """
    Checks if all experiment folders exists and returns the number of experiments.  

    Parameters
    ----------
    data_path : string
        base path where the experiment folders are stored
    explist : array_like
        array of experiment names (names of the folders)

    Returns
    ----------
    explist : array 
    len(explist): integer
    """
    for exp in explist:
        if not path.isdir(data_path +"/" +exp):
            print("ERROR: " +exp +": directory " +data_path +"/" +exp +" does not exist!") 
            return None
        else:
            print(exp +": directory is " +data_path +"/" +exp)
    return explist, len(explist)


def get_explist_icona(data_path, explist):
    """
    Checks if all experiment files exists and returns the number of experiments.  

    Parameters
    ----------
    data_path : string
        base path where the experiment folders are stored
    explist : array_like
        array of experiment names 

    Returns
    -------
    explist : array 
    len(explist): integer
    """
    for exp in explist:
        fpath = data_path +"/" +exp +"_atm_2d_ml.mm.gm.nc"
        if not path.isfile(fpath):
            print("ERROR: " +fpath +" does not exist!") 
            return None
        else:
            print(exp +" is in " +data_path +"/")
    return explist, len(explist)


def load_ds_2d(data_path, explist, timesdecode=False, droplast=True ): 
    """
    Loads the 2d monthly mean zonal & global mean data  

    Parameters
    ----------
    data_path : string
        base path where the experiment folders are stored
    explist : array_like
        array of experiment names (names of the folders)

    Returns
    -------
    DSlistgm: array of 2d global mean datasets 
    DSlistzm: array of 2d zonal mean datasets
    """
    DSlistgm=np.empty(len(explist),dtype="object")
    DSlistzm=np.empty(len(explist),dtype="object")
    for i, exp in enumerate(explist):
        DS=xr.open_dataset(data_path +"/" +exp +"/" +exp + "_atm_2d_ml.mm.gm.nc", decode_times=timesdecode)
        if timesdecode==False:
            DS=DS.assign_coords(time=(DS.time/360))
        if droplast:
            DS=DS.isel(time=slice(0,-1))
        DSlistgm[i]=DS

        DS=xr.open_dataset(data_path +"/" +exp +"/" +exp + "_atm_2d_ml.mm.zm.nc", decode_times=timesdecode)
        if timesdecode==False:
            DS=DS.assign_coords(time=(DS.time/360))
        if droplast:
            DS=DS.isel(time=slice(0,-1))
        DSlistzm[i]=DS
    return DSlistgm, DSlistzm


def load_ds_3d(data_path, explist, timesdecode=False, droplast=True): 
    """
    Loads the 3d monthly mean zonal  data  

    Parameters
    ----------
    data_path : string
        base path where the experiment folders are stored
    explist : array_like
        array of experiment names (names of the folders)

    Returns
    -------
    DSlistzmml: array of 3d zonal mean datasets on model levels
    DSlistzmpl: array of 3d zonal mean datasets on pressure levels
    """
    DSlistzmml = np.empty(len(explist), dtype="object")
    DSlistzmpl = np.empty(len(explist), dtype="object")
    for i, exp in enumerate(explist):
        DSml = xr.open_mfdataset(data_path+"/"+exp+"/"+exp+"_atm_3d_ml*.ps.mm.zm.nc", decode_times = timesdecode)
        DSpl = xr.open_mfdataset(data_path+"/"+exp+"/"+exp+"_atm_3d_pl*.ps.mm.zm.nc", decode_times = timesdecode)
        if timesdecode==False:
            DSml=DSml.assign_coords(time=(DSml.time/360))
            DSpl=DSpl.assign_coords(time=(DSpl.time/360))

        if droplast:
            DSml=DSml.isel(time=slice(0,-1))
            DSpl=DSpl.isel(time=slice(0,-1))
            
        DSlistzmml[i]=DSml
        DSlistzmpl[i]=DSpl
    return  DSlistzmml, DSlistzmpl

def load_ds_2d_icona(data_path, explist, timesdecode=False, droplast=True ): 
    """
    Loads the 2d monthly mean zonal & global mean data from ICON-A simulations from mistral.

    Parameters
    ----------
    data_path : string
        base path where the experiment folders are stored
    explist : array_like
        array of experiment names (names of the folders)

    Returns
    -------
    DSlistgm: array of 2d global mean datasets 
    DSlistzm: array of 2d zonal mean datasets
    """
    DSlistgm=np.empty(len(explist),dtype="object")
    DSlistzm=np.empty(len(explist),dtype="object")
    for i, exp in enumerate(explist):
        DS=xr.open_dataset(data_path +"/" +exp + "_atm_2d_ml.mm.gm.nc", decode_times=timesdecode)
        if timesdecode==False:
            DS=DS.assign_coords(time=(DS.time/360))
        if droplast:
            DS=DS.isel(time=slice(0,-1))
        DSlistgm[i]=DS

        DS=xr.open_dataset(data_path +"/"  +exp + "_atm_2d_ml.mm.zm.nc", decode_times=timesdecode)
        if timesdecode==False:
            DS=DS.assign_coords(time=(DS.time/360))
        if droplast:
            DS=DS.isel(time=slice(0,-1))
        DSlistzm[i]=DS
    return DSlistgm, DSlistzm



def sictoicelat(sic):
    """
    Extremely sophisticated trigonometric calculations to get the ice-edge latitude from a fractional ice cover, with the assumption that the ice border is parallel to meridians and symmetric with the respect to the equator.

    Parameters
    ----------
    sic : array_like
        array of fractional sea-ice cover (between 0 and 1)

    Returns
    -------
    icelat: array of ice-edge latitudes in degree
    """
    return np.rad2deg(np.arcsin(1-sic))


def icelatosic(icelat):
    """
    Extremely sophisticated trigonometric calculations to get the fractional ice cover from an ice-edge latitude , with the assumption that the ice border is parallel to meridians and symmetric with the respect to the equator.

    Parameters
    ----------
    icelat : array_like
        array of ice-edge latitudes in degree

    Returns
    -------
    sic: array of fractional sea-ice cover (between 0 and 1) 
    """
    return 1-np.sin(np.deg2rad(icelat))


def get_albedo(dataset, albtype):
    """
    Calculate albedo at TOA/surface with/without clouds from ICON data.
    ----------
    dataset : xarray dataset
        Dataset that contains the radiative variables from an ICON simulation.
    albtype : string
        Where albedo should be calculated: toa, toacs, surf, surfcs
    Returns
    -------
    da_albedo: Dataarray that contains the albedo values. NaN if there is no incoming solar radiation.
    """
    if albtype=="toa":
        return (dataset["rsut"]/dataset["rsdt"]).persist()
    elif albtype=="toacs":
        return (dataset["rsutcs"]/dataset["rsdt"]).persist()
    elif albtype=="surf":
        return (dataset["rsus"]/dataset["rsds"]).persist()
    elif albtype=="surfcs":
        return (dataset["rsuscs"]/dataset["rsdscs"]).persist()
    else:
        print("ERROR: albtype has to be one of: toa, toacs, surf, surfcs")
        exit()


def get_cre(dataset, cretype, radtype):
    """
    Calculate CRE at TOA/surface with/without clouds from ICON data.

    Parameters
    ----------
    dataset : xarray dataset
        Dataset that contains the radiative variables from an ICON simulation.
    cretype : string
        Where CRE should be calculated: toa, surf, atm.
    radtype : string
        What radiation should be used: sw, lw, net.

    Returns
    -------
    da_cre: Dataarray that contains the CRE values. Positive downward in W/m^2.
    """
    da_cre = None
    if cretype=="toa":
        CRESW = dataset["rsutcs"]-dataset["rsut"]

        if radtype=="net":
            CRELW = dataset["rlutcs"]-dataset["rlut"]
            da_cre = CRESW+CRELW
        elif radtype=="sw":
            da_cre = CRESW
        elif radtype=="lw":
            CRELW = dataset["rlutcs"]-dataset["rlut"]
            da_cre = CRELW
        else: 
            print("ERROR: radtyp has to be one of: sw, lw, net")
            exit()
    elif cretype=="surf":
        CRESW = dataset["rsds"]-dataset["rsus"] - (dataset["rsdscs"]-dataset["rsuscs"])
        
        if radtype=="net":
            CRELW = dataset["rluscs"]-dataset["rlus"]
            da_cre = CRESW+CRELW
        elif radtype=="sw":
            da_cre = CRESW
        elif radtype=="lw":
            CRELW = dataset["rluscs"]-dataset["rlus"]
            da_cre = CRELW
        else: 
            print("ERROR: radtyp has to be one of: sw, lw, net")
            exit()
    elif cretype=="atm":
        CRESW_toa = dataset["rsut"]-dataset["rsutcs"]
        CRELW_toa = dataset["rlut"]-dataset["rlutcs"]

        CRESW_surf = dataset["rsds"]-dataset["rsus"] - (dataset["rsdscs"]-dataset["rsuscs"])
        CRELW_surf = dataset["rluscs"]-dataset["rlus"]

        if radtype=="net":
            da_cre = CRESW_toa+CRELW_toa - (CRESW_surf+CRELW_surf)
        elif radtype=="sw":
            da_cre = CRESW - CRESW_surf
        elif radtype=="lw":
            da_cre = CRELW - CRELW_surf
        else: 
            print("ERROR: radtyp has to be one of: sw, lw, net")
            exit()

    else:
        print("ERROR: cretype has to be one of: toa, surf, atm")
        exit()

    da_cre.attrs["units"]="Wm-2"
    return da_cre


def get_toaeb(dataset, ebtype="as"):
    """
    Calculate the TOA energy balance from ICON data.

    Parameters
    ----------
    dataset : xarray dataset
        Dataset that contains the radiative variables from an ICON simulation.
    ebtype : string
        All sky or clear sky toa eb (as or cs)

    Returns
    -------
    da_toaeb: Dataarray that contains the toa eb values. Positive downward in W/m^2.
    """
    if ebtype=="as":
        return dataset["rsdt"]-dataset["rsut"]-dataset["rlut"]
    elif ebtype=="cs":
        return dataset["rsdt"]-dataset["rsutcs"]-dataset["rlutcs"]
    else:
        print("ERROR: ebtype has to be one of: as, cs")
        exit()


def get_eb(dataset, location="TOA", ebtype="as"):
    """
    Calculate the TOA energy balance from ICON data.

    Parameters
    ----------
    dataset : xarray dataset
        Dataset that contains the energy fluxes from an ICON simulation.
    location : string
        TOA, SURF or ATM. Where the energy balance should be calculated. 
    ebtype : string
        All sky or clear sky eb (as or cs)

    Returns
    -------
    da_toaeb: Dataarray that contains the eb values. Positive downward in W/m^2.
    """

    if ebtype=="as":
        toaeb = dataset["rsdt"]-dataset["rsut"]-dataset["rlut"]

        surfeb=dataset["rsds"] + dataset["rlds"] + dataset["hfss"] + dataset["hfls"] 
        - dataset["rsus"] - dataset["rlus"]
    elif ebtype=="cs":
        toaeb = dataset["rsdt"]-dataset["rsutcs"]-dataset["rlutcs"]

        #surfeb=dataset["rsdscs"] + dataset["rldscs"] + dataset["hfss"] + dataset["hfls"] 
        #- dataset["rsuscs"] - dataset["rluscs"]
    else:
        print("ERROR: ebtype has to be one of: as, cs")
        exit()

    if location=="SURF":
        return surfeb
    elif location=="TOA":
        return toaeb
    elif location=="ATM":
        return toaeb - surfeb
    else:
        print("ERROR: location has to be one of: TOA, SURF, ATM")
        exit()

    

    if ebtype=="as":
        return dataset["rsdt"]-dataset["rsut"]-dataset["rlut"]
    elif ebtype=="cs":
        return dataset["rsdt"]-dataset["rsutcs"]-dataset["rlutcs"]
    else:
        print("ERROR: ebtype has to be one of: as, cs")
        exit()


def get_evap_ice(dataset):
    """
    Calculate the fraction of the evaporative flux that consists of solid ice/snow sublimating. evspsbl in ICON consists of both surface ice and water sublimating/evaporating in kg/(m^2s). 

    Parameters
    ----------
    dataset : xarray dataset
        Dataset that contains the evaporative flux evspsbl in kg/m^2s from an ICON simulation.

    Returns
    -------
    evap_icefrac: Dataarray that containts the fraction of ice sublimating. Water evaporating is 1-evap_icefrac.
    """

    # from ICON
    alv   = 2.5008e6     # [J/kg]   latent heat for vaporisation
    als   = 2.8345e6      # [J/kg]   latent heat for sublimation

    L_global = (dataset["hfls"] / dataset["evspsbl"] ) # global mean latent heat of vaporisation/sublimation

    evap_icefrac=(L_global-alv) / (als - alv)
    return evap_icefrac


def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    newcolor=np.asarray(colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2]))

    # fix values out of range
    newcolor[(newcolor>1.0)] = 1.0
    newcolor[(newcolor<0.0)] = 0.0
    return newcolor


def scale_color_co2(color, co2, co2min=4000, co2max=10000, lummin=0.5, lummax=1.5):
    """
    Scales the color by the given co2 value. The luminosity is scaled linearly between lummin and lummax.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Parameters
    ----------
    color : string
        matplotlib color string, hex string, or RGB tuple
    co2 : float
        co2 value
    co2min : float
        minimum co2 value
    co2max : float
        maximum co2 value
    lummin : float
        minimum luminosity
    lummax : float
        maximum luminosity  

    Returns
    -------
    newcolor: RGB tuple
    """
    a = (lummax-lummin) / (float(co2max)-float(co2min))
    b = lummin -float(co2min) * a
    return lighten_color(color, amount=a*float(co2)+b)


def calc_spline(x, y, xnew, smoothing=1, degree=1): 
    """
    Calculate 1d spline interpolation to a new array. Dublicates in x are removed. If xnew is larger than x, ynew values outside are set to NaN.

    Parameters
    ----------
    x : array_like 
        1d array that contains the original x values (where the spline interpolation takes place)
    y : array_like 
        1d array that contains the original y values
    xnew : array_like 
        1d array that contains the new x values where the values should be interpolated to
    smoothing : integer 
        smoothing factor of the spline calculation (see documentation of scipy.interpolate.UnivariateSpline), default 1
    degree : integer 
        degree of the spline interpolation, 1 (default) is linear 

    Returns
    -------
    ynew: the input array y interpolated to xnew, same shape as xnew
    """
    from scipy.interpolate import UnivariateSpline # import module 


    _, ind1=np.unique(x,return_index=True) # remove dublicate x values
    ind2=np.squeeze(np.argwhere(~np.isnan(x.values))) # remove nans
    ind = np.intersect1d(ind1,ind2)

    data=[]
    data1=[]

    data = np.vstack([x[ind].values,y[ind].values])
    data1=data[:,data[0].argsort()] # sort data for ascending x

    f = UnivariateSpline(data1[0], data1[1], s=smoothing, k=degree, ext=1) # make spline interpolation

    xrange=[min(x.values),max(x.values)]
    ynew = f(xnew) 
    ynew[(xnew>xrange[1]) | (xnew<xrange[0])]=np.nan # set values outside of original xbounds to nan

    return ynew


def get_forcing_co2(co2_fac, alpha=5.35):    
    """
    Calculate estimated radiative forcing from a change in atmospheric co2 (see e.g Myhre et al., 1998). dF=alpha*ln(CO2/CO2_0)

    Parameters
    ----------
    co2_fac: float 
        Relative change in CO2 content, co2_fac=co2/co2_0
    alpha : float 
        factor that relates CO2 to radiative forcing, in W/m^2       

    Returns
    -------
    dF: change in radiative forcing in W/m^2
    """
    return alpha*np.log(co2_fac)


def get_co2_forcing(dF, alpha=5.35):
    """
    Calculate estimated relative co2 change from a given radiative forcing. (see e.g Myhre et al., 1998). CO2_fac=CO2/CO2_0=exp(dF/alpha)

    Parameters
    ----------
    dF: float 
        Change in radiative forcing attributed to CO2, in W/m^2
    alpha : float 
        factor that relates CO2 to radiative forcing, in W/m^2         

    Returns
    -------
    CO2_fac: relative change in CO2 content, CO2_fac=CO2/CO2_0
    """
    return np.exp(np.divide(dF,alpha))


def find_co2_expname(expname):
    startind=expname.find("_")+1
    endind=expname[startind:].find("_")
    return expname[startind:startind+endind]

def find_co2_expname_vscicona(expname):
    temp=expname.find("_")+1
    startind = expname.find("_", temp+1)+1 # second occurance
    endind=expname[startind:].find("_")
    return expname[startind:startind+endind]


def find_co2_expname_icona(expname):    
    temp = expname.find("_")
    startind = expname.find("_", temp+1)+1 # second occurance
    endind=expname[startind:].find("ppmv")
    return expname[startind:startind+endind]

def weighted_mean_dim(DS, weights, dim="lat"):
    """
    Calculate the weighted mean of a dataset along a dimension.

    Parameters
    ----------
    DS : xarray dataset
        Dataset that contains the variables lat and lon.
    weights : array like
        array that contains the weights for the weighted mean.
    dim : string
        Dimension along which the weighted mean is calculated.

    Returns
    -------
    weighted_mean: weighted mean of the dataset along the dimension dim    
    """
    return ((DS * weights).sum(dim=dim) / sum(weights)).squeeze()


def mean_climatezones(DS, lat_trop, lat_subtrop):
    """
    Calculate the mean of a dataset for the tropics, subtropics and extratropics. Tropics  are between -lat_trop and lat_trop, subtropics between lat_trop and lat_subtrop and extratropics between lat_subtrop and 90.

    Parameters
    ----------
    DS : xarray dataset
        Dataset that contains the coordinate lon.
    lat_trop : float
        Latitude of the border between tropics and subtropics.
    lat_subtrop : float
        Latitude of the border between subtropics and extratropics.

    Returns
    -------
    DS_trop: mean of the dataset for the tropics
    DS_subtrop: mean of the dataset for the subtropics
    DS_extratrop: mean of the dataset for the extratropics
    """

    DS_trop = DS.where((DS.lat <= lat_trop) & (DS.lat >= -1*lat_trop), drop=True)
    weight_trop = np.cos(np.deg2rad(DS_trop.lat))
    print("tropics: "  + str(DS_trop.lat.count().values) + " points between "  + str(lat_trop) + "° and -" + str(lat_trop) + "°")
    DS_subtrop = DS.where(((DS.lat <= lat_subtrop) & (DS.lat > lat_trop)) | ((DS.lat >= -1*lat_subtrop) & (DS.lat < -1*lat_trop)), drop=True)
    weight_subtrop = np.cos(np.deg2rad(DS_subtrop.lat))
    print("subtropics: " + str(DS_subtrop.lat.count().values) + " points between +-"  + str(lat_trop) + "° and +-" + str(lat_subtrop) + "°")
    DS_extratrop = DS.where((DS.lat >= lat_subtrop) | (DS.lat <= -1*lat_subtrop), drop=True)
    weight_extratrop = np.cos(np.deg2rad(DS_extratrop.lat))
    print("extratropics: " + str(DS_extratrop.lat.count().values) + " points between +-"  + str(lat_subtrop) + "° and +-90°")

    DS_trop = weighted_mean_dim(DS_trop, weight_trop)
    DS_subtrop = weighted_mean_dim(DS_subtrop, weight_subtrop)
    DS_extratrop = weighted_mean_dim(DS_extratrop, weight_extratrop)

    return DS_trop, DS_subtrop, DS_extratrop

    