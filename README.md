Code repository for the manuscript "Sea-ice thermodynamics can determine waterbelt scenarios for Snowball Earth" to Earth System Dynamics. Data required is available at https://doi.org/10.25365/phaidra.429. 

### Content
- ICON-runscripts: runscripts for all simulations performed with ICON-A 
- cdo-postprocessing: shell scripts used for postprocessing with cdo
- python-figures: jupyter notebooks used for creating the figures of the manuscript, python script with helper functions, figures

### History
- 09.09.2023 Initial submission
- 22.01.2024 Submitted first revision
- 06.02.2024 Accepted