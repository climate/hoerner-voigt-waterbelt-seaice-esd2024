#! /bin/ksh
#=============================================================================
#SBATCH --account=p71386
#SBATCH --partition=skylake_0384
#SBATCH --job-name=ICON_pp_2d
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=48
#SBATCH --ntasks-per-core=1
#SBATCH --time=00:30:00
#SBATCH --mail-user=johannes.hoerner@univie.ac.at

# Script for initial post processing routines for ICON simulations on VSC
# it will create monthyl, global & zonal means of 2D data 
# additionally, the fractional snow cover for each grid point will be calculated from hs_icecl

logfile="${PWD}/logfiles/LOG.pp2d.${1}";
exec 2>&1 | tee -a $logfile;

echo "$(date '+%Y-%m-%d %H:%M:%S') postprocessing 2d data for $1" | tee -a $logfile;

if [ -z "$1" ]
  then
    echo "no experiment given!" | tee -a $logfile;
    exit | tee -a $logfile;
fi


experiment=$1;

# input & output directory
datapath="/gpfs/data/fs71767/jhoerner/experiments/$experiment";
outpath="/gpfs/data/fs71767/jhoerner/postprocessing/$experiment";

# create output directory
if [ ! -d "$outpath" ] 
then
    echo "creating directory ${outpath}" | tee -a $logfile;
    mkdir $outpath | tee -a $logfile;
fi


# add trailing slash if needed
length=${#datapath};
last_char=${datapath:length-1:1};

[[ $last_char != "/" ]] && datapath="$datapath/";

echo "input path is $datapath" | tee -a $logfile;
echo "output path is $outpath/" | tee -a $logfile;


mergedfile_temp="${datapath}${experiment}_atm_2d_ml_merged_temp.nc";
mergedfile="${datapath}${experiment}_atm_2d_ml_merged.nc";
gmfile="${outpath}/${experiment}_atm_2d_ml.mm.gm.nc";
gmymfile="${outpath}/${experiment}_atm_2d_ml.ym.gm.nc";
zmfile="${outpath}/${experiment}_atm_2d_ml.mm.zm.nc";
zmymfile="${outpath}/${experiment}_atm_2d_ml.ym.zm.nc";

echo "merging files..." | tee -a $logfile;
cdo -O mergetime "${datapath}${experiment}_atm_2d_ml_0*Z.nc" $mergedfile_temp | tee -a $logfile;

# calculate snowfrac
# the albedo scheme sees ice as snow covered if hs*rho_ref/rhos>0.01m <-> hs>rhos/rho_ref * 0.01m = 0.0341674m (see mo_ice_parameterizations.f90)
echo "calculating fractional snowcover" | tee -a $logfile;
cdo -O aexpr,'snowfrac=hs_icecl>0.0341674' $mergedfile_temp $mergedfile | tee -a $logfile;

echo "creating global & monthly & yearly mean..." | tee -a $logfile;
cdo -O monmean -fldmean  $mergedfile $gmfile | tee -a $logfile;
cdo -O yearmean  $gmfile $gmymfile| tee -a $logfile;

echo "creating zonal & monthly & yearly mean..." | tee -a $logfile;
cdo -O zonmean -remapcon,r192x96 -monmean $mergedfile $zmfile | tee -a $logfile;
cdo -O yearmean  $zmfile $zmymfile| tee -a $logfile;

echo "deleting temporary files" | tee -a $logfile;
rm $mergedfile | tee -a $logfile;
rm $mergedfile_temp | tee -a $logfile;

echo "done" | tee -a $logfile;
