#!/bin/bash
data_path="/gpfs/data/fs71386/jhoerner/inputdata/aquaplanet/sst_and_seaice"
echo "directory is ${data_path}"
cd $data_path

cdo -O timmean sst_R02B04_G.1979-2008.nc sst_R02B04_G.1979-2008.tm.nc
cdo -O remapdis,r180x90 sst_R02B04_G.1979-2008.tm.nc sst_R02B04_G.1979-2008.tm.rm.nc

cdo -O invertlatdes -invertlat sst_R02B04_G.1979-2008.tm.rm.nc sst_R02B04_G.1979-2008.tm.rm.inv.nc

cdo -O ensmean sst_R02B04_G.1979-2008.tm.rm.nc sst_R02B04_G.1979-2008.tm.rm.inv.nc sst_R02B04_G.1979-2008.tm.rm.eqsym.nc 

cdo -O zonmean sst_R02B04_G.1979-2008.tm.rm.eqsym.nc  sst_R02B04_G.1979-2008.tm.rm.eqsym.zm.nc
cdo -O enlarge,r180x90 sst_R02B04_G.1979-2008.tm.rm.eqsym.zm.nc sst_R02B04_G.1979-2008.tm.rm.eqsym.zm.en.nc
cdo -O remapdis,/gpfs/data/fs71386/jhoerner/inputdata/aquaplanet/grids/icon_grid_0005_R02B04_G.nc sst_R02B04_G.1979-2008.tm.rm.eqsym.zm.en.nc sst_R02B04_G.1979-2008.final.nc
