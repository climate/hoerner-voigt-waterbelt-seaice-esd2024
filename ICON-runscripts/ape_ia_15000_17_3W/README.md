# ape_ia_15000_17_3W
Winton run with ICON-A, branch off from ape_ia_8000_90_3W, year 220. All settings the same as there (except CO2...)  Damping parameter is 10. Time step is 6 min. csecfrl = 5e-5. Auto restart is enabled from the start. 
restart file ape_ia_8000_90_3W_restart_atm_02200101T000000Z.nc

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 12.05.23 14:45 - simulation start at year 1, end year 100
- 23-06-2023 10:18:12 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 10:37:09 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 10:55:03 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 11:13:19 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 11:31:13 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 11:49:10 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 12:07:16 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 12:25:08 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 12:43:10 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 13:01:19 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 13:19:18 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 13:37:06 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 13:55:23 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 14:15:08 crash, simulation is in Snowball, stop (238-10-23)
