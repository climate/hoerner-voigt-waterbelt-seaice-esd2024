# ape_ia_6000_90_0S
First production run using ICON-A. I use the same initial conditions and boundary conditions as for the ICON-ESM runs, EXCEPT OZONE. For ozone, I use the same file as the ICON-A runs on mistral, because there was a change in the conventions used. I set the damping parameter to 10 from the start. Time step is 10 min. This simulation is expected to fall into a waterbelt (compare to Braun et al. 2022, simulation exp.mlo_aqua_6000ppmv_BFP.run. As in that simulation, csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

2 nodes
account p71386 (institute nodes)
15 hours time with 5 years restart interval


## explog
20.03.23 16:30 - simulation start at year 1, end year 100
- 20-03-2023 19:36:57 crash, autorestart with hdiff_smag_fac=0.015001
- 21-03-2023 11:34:23 crash, autorestart with hdiff_smag_fac=0.015 
- 21-03-2023 14:01:12 crash, autorestart with hdiff_smag_fac=0.015001
- 21-03-2023 14:48:10 crash, autorestart with hdiff_smag_fac=0.015 (manually changed timestep to 8min)
- 22-03-2023 12:15:49 crash, autorestart with hdiff_smag_fac=0.015001
- 27-03-2023 00:14:15 crash, autorestart with hdiff_smag_fac=0.015
- 27-03-2023 03:44:55 crash, autorestart with hdiff_smag_fac=0.015001
- 27-03-2023 09:49:09 crash, autorestart with hdiff_smag_fac=0.015
- 27-03-2023 12:35:51 crash, autorestart with hdiff_smag_fac=0.015001
- 27-03-2023 22:18:11 crash, autorestart with hdiff_smag_fac=0.015
- 28-03-2023 01:04:02 crash, autorestart with hdiff_smag_fac=0.015001
- 28-03-2023 04:36:10 crash, autorestart with hdiff_smag_fac=0.015
- 28-03-2023 06:21:40 crash, autorestart with hdiff_smag_fac=0.015001
- 28-03-2023 06:25:34 crash, autorestart with hdiff_smag_fac=0.015
- 28-03-2023 07:32:52 crash, autorestart with hdiff_smag_fac=0.015001
- 28-03-2023 14:30 finish on 100-01-01, continue until 200-01-01 with timestep=6m
- 06-04-2023 10:30 finish on 200-01-01, continue until 250-01-01
- 11-04-2023 01:00 finish on 250-01-01
- 11-04-2023 15:00 continue until 300-01-01
- 15-04-2023 22:49:11 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:50:08 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:52:07 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:54:07 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:56:07 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:58:07 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:00:09 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:02:09 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:04:09 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:06:09 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:08:09 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:10:10 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:12:10 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:14:10 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:16:10 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:18:10 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:20:10 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:22:11 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:24:11 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:26:12 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:28:12 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:30:13 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:32:13 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:34:13 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:36:13 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:38:13 crash, autorestart with hdiff_smag_fac=0.015001, DISK QUOTA EXCEEDED!!! (294-08)
- 17-04-2023 15:40 continue
- 18-04-2023 04:00 finish at 300-01-01
