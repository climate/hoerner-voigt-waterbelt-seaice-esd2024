# Snowball simulations on an aquaplanet using ICON-A


Parent folder of all Snowball simulations in an aquaplanet setup with ICON-A. Compare to ape_snowball, which are the same simulations but with ICON-ES;

Setup is: aquaplanet, Semtner & Winton sea-ice model, adapted Bergeron-Findeisen-Process (csecfrl = 5e-5), circular Kepler orbit with 23.5° obliquity, `TSI=0.9442*1360.875~=1285 W/m^2`, snow albedo 0.79-0.66, ice albedo 0.45-0.38, linearly interpolated from Tmelt to Tmelt-1K. 

Starting timestep is 10m, with rayleigh_coeff=0.1. If the model becomes unstable, the timestep will be decreased to 8m and rayleigh_coeff=10.

The naming convention is: 

`ape_ia_{co2-content}_{initial sea-ice-edge latitude}`

runscripts are adapted from H�rner et al. (2022) and Braun et al. (2022)
