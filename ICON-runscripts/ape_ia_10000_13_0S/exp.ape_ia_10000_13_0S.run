#! /bin/ksh
#=============================================================================
#SBATCH --account=p71767
#SBATCH --partition=skylake_0096
#SBATCH --job-name=ape_ia_10000_13_0S
#SBATCH --nodes=5
#SBATCH --ntasks-per-node=48
#SBATCH --ntasks-per-core=1
#SBATCH --output=/home/fs71767/jhoerner/runscripts/snowball_ape_ia/ape_ia_10000_13_0S/logfiles/LOG.exp.ape_ia_10000_13_0S.run.%j.o
#SBATCH --error=/home/fs71767/jhoerner/runscripts/snowball_ape_ia/ape_ia_10000_13_0S/logfiles/LOG.exp.ape_ia_10000_13_0S.run.%j.o
#SBATCH --exclusive
#SBATCH --time=24:00:00
#SBATCH --mail-user=johannes.hoerner@univie.ac.at
#SBATCH --mail-type=BEGIN,END,FAIL

set -x # debugging command: enables a mode of the shell where all executed commands are printed to the terminal
ulimit -s unlimited # unsets limits for RAM

# MPI variables
# -------------
no_of_nodes=5
mpi_procs_pernode=48
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))
#
# blocking length
# ---------------
nproma=16



#=============================================================================
# Input variables:

# SIMULATION NAME
EXP=ape_ia_10000_13_0S

ICONFOLDER=/home/fs71767/jhoerner/icon-a       # DIRECTORY OF ICON MODEL CODE
RUNSCRIPTDIR=/home/fs71767/jhoerner/runscripts/snowball_ape_ia/${EXP}
INDIR=/gpfs/data/fs71767/jhoerner/inputdata/aquaplanet    # directory with input data
basedir=$ICONFOLDER # icon base directory

. ${ICONFOLDER}/run/add_run_routines

# experiment directory, with plenty of space, create if new
EXPDIR=/gpfs/data/fs71767/jhoerner/experiments/${EXP}
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
#
ls -ld ${EXPDIR}
if [ ! -d ${EXPDIR} ] ;  then
    mkdir ${EXPDIR}
fi
ls -ld ${EXPDIR}

cd $EXPDIR




#=================================================================================
# dictionary file for output variable names
dict_file="dict.${EXP}"
cat $ICONFOLDER/run/dictfiles/dict.iconam.mpim  > ${dict_file}

# the namelist filename
atmo_namelist=NAMELIST_${EXP}_atm
lnd_namelist=NAMELIST_${EXP}_lnd


#-----------------------------------------------------------------------------
# global timing
start_date="0001-01-01T00:00:00Z" # format of specified model time is YYYY-MM-DDTHH:MM:SSZ
end_date="0350-01-01T00:00:00Z"


# restart intervals
restart_interval="P20Y"
checkpoint_interval="P6M"

# output intervals
output_interval="P1M"
file_interval="P1M"


#-----------------------------------------------------------------------------
# model timing
dtime="PT6M" # 360 sec for R2B6, 120 sec for R3B7



#================================================================================
# Link the input files

# range of years for yearly files
# assume start_date and end_date have the format yyyy-...
start_year=$(( ${start_date%%-*} - 1 ))
end_year=$(( ${end_date%%-*} + 1 ))

# grid
atmo_dyn_grids="iconR02B04-grid.nc"
#ln -sf $INDIR/grids/icon_grid_0005_R02B04_G.nc ${atmo_dyn_grids} #grid
add_link_file $INDIR/grids/icon_grid_0005_R02B04_G.nc ${atmo_dyn_grids}

#file with some precission definitions
#ln -sf  $ICONFOLDER/data/lsdata.nc lsdata.nc

# boundary conditions atmosphere
#ln -sf $INDIR/sst_and_seaice/sic_R02B04_aqua.nc bc_sic.nc
#ln -sf $INDIR/sst_and_seaice/sst_R02B04_aqua.nc bc_sst.nc
add_link_file $INDIR/sst_and_seaice/sic_R02B04_aqua.nc ./bc_sic.nc
add_link_file $INDIR/sst_and_seaice/sst_R02B04_aqua.nc ./bc_sst.nc

# initial conditions atmosphere
ifsfile="ifs2icon.nc"
#ln -sf $INDIR/ifs/ifs2icon_1979010100_R02B04_G_aqua.nc ${ifsfile} # initial conditions
add_link_file $INDIR/ifs/ifs2icon_1979010100_R02B04_G_aqua.nc ${ifsfile}


# boundary conditions ozone
#ln -sf $INDIR/ozone/ape_o3_iconR2B04-ocean_aqua_planet.nc          ./o3_icon_DOM01.nc
add_link_file $INDIR/ozone/ape_o3_iconR2B04-ocean_aqua_planet.nc               ./o3_icon_DOM01.nc

# aerosols
# tropospheric anthropogenic aerosols, simple plumes
# ln -sf $BASEDIR/data/MACv2.0-SP_v1.nc MACv2.0-SP_v1.nc
# boundary conditions hitoric background aerosol (Kinne 1850)
# ln -sf $INDIR/aerosol/bc_aeropt_kinne_lw_b16_coa.nc bc_aeropt_kinne_lw_b16_coa.nc
# ln -sf $INDIR/aerosol/bc_aeropt_kinne_sw_b14_coa.nc bc_aeropt_kinne_sw_b14_coa.nc

#year=$start_year
#while [[ $year -le $end_year ]]
#do
#  ln -sf $INDIR/aerosol/bc_aeropt_kinne_sw_b14_fin_2000.nc bc_aeropt_kinne_sw_b14_fin_${year}.nc
#  (( year = year+1 ))
#done

# Cloud optical properties
#ln -sf $ICONFOLDER/data/ECHAM6_CldOptProps.nc ECHAM6_CldOptProps.nc
add_link_file $ICONFOLDER/data/rrtmg_lw.nc                              ./rrtmg_lw.nc
add_link_file $ICONFOLDER/data/rrtmg_sw.nc                              ./rrtmg_sw.nc
add_link_file $ICONFOLDER/data/ECHAM6_CldOptProps.nc                    ./ECHAM6_CldOptProps.nc

# land
# initial conditions land
#ln -sf $INDIR/land/ic_land_soil_aqua.nc ic_land_soil.nc
add_link_file $INDIR/land/ic_land_soil_aqua.nc ./ic_land_soil.nc


# JSBACH settings --> land model (part of atmo model)
run_jsbach=yes
jsbach_usecase=jsbach_lite    # jsbach_lite or jsbach_pfts
jsbach_with_lakes=yes
jsbach_with_hd=no
jsbach_with_carbon=no         # yes needs jsbach_pfts usecase
jsbach_check_wbal=no          # check water balance
# Some further processing for land configuration
ljsbach=$([ "${run_jsbach:=no}" == yes ] && echo .TRUE. || echo .FALSE. )
llake=$([ "${jsbach_with_lakes:=yes}" == yes ] && echo .TRUE. || echo .FALSE. )
lcarbon=$([ "${jsbach_with_carbon:=yes}" == yes ] && echo .TRUE. || echo .FALSE. )
#
# boundary conditions land 
#ln -sf $INDIR/land/bc_land_sso_aqua.nc bc_land_sso.nc # subgrid scale orography
#ln -sf $INDIR/land/bc_land_frac_aqua.nc bc_land_frac.nc
#ln -sf $INDIR/land/bc_land_phys_aqua.nc bc_land_phys.nc
#ln -sf $INDIR/land/bc_land_soil_aqua.nc bc_land_soil.nc
add_link_file $INDIR/land/bc_land_sso_aqua.nc ./bc_land_sso.nc
add_link_file $INDIR/land/bc_land_frac_aqua.nc ./bc_land_frac.nc
add_link_file $INDIR/land/bc_land_phys_aqua.nc ./bc_land_phys.nc
add_link_file $INDIR/land/bc_land_soil_aqua.nc ./bc_land_soil.nc

# - lctlib file for JSBACH
add_link_file ${ICONFOLDER}/externals/jsbach/data/lctlib_nlct21.def        ./lctlib_nlct21.def

# print_required_files
copy_required_files
link_required_files



# initialize diffusion parameter for automatic restart from crashes
if [ -r hdiff_smag_fac_offset ]; then
  diff_fac_offset=`awk '{ print }' hdiff_smag_fac_offset` #read the offset from file
else
  diff_fac_offset=0.0
fi

init_diff_fac=$(echo $diff_fac_offset + 0.015 | bc)


# model parameters
model_equations=3
  # equation system
#                     1=hydrost. atm. T
#                     1=hydrost. atm. theta dp
#                     3=non-hydrost. atm.,
#                     0=shallow water model
#                    -1=hydrost. ocean

# calendar type:   'proleptic gregorian'->type 1 (default), '365 day year'->type 2, '360 day year' -> type 3
calendar='360 day year'
calendar_type=2 # Namelist overview seems to be wrong. 2 is 360 day year.
		# In shared/mo_impl_constants.f90
		#!------------------------!
		#!  CALENDAR TYPES        !
  		#!------------------------!

  		#INTEGER,  PARAMETER :: julian_gregorian    = 0 !< historic Julian / Gregorian
  		#INTEGER,  PARAMETER :: proleptic_gregorian = 1 !< proleptic Gregorian
  		#INTEGER,  PARAMETER :: cly360              = 2 !< constant 30 dy/mo and 360 dy/yr


#-----------------------------------------------------------------------------
# automatic restart setup
# set some default values and derive some run parameteres
restart=${restart:=".false."}
restartSemaphoreFilename='isRestartRun.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP

# wait 5min to let GPFS finish the write operations
if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
    sleep 10;
  fi
fi


#==============================================================================
# create ICON master namelist
# ------------------------

# For a complete list see Namelist_overview and Namelist_overview.pdf
cat > icon_master.namelist << EOF
&master_nml
 lrestart            = ${restart}
/
&master_model_nml
  model_name="atmo"
  model_type=1	!model types:
		! 1 = atmsphere
		! 2 = ocean
		! 3 = radiation
  		! 99 = dummy
  model_namelist_filename="${atmo_namelist}"
  model_type=1
  model_min_rank=0
  model_max_rank=65535
  model_inc_rank=1
/
&jsb_model_nml
 model_id = 1
 model_name = "JSBACH"
 model_shortname = "jsb"
 model_description = 'JSBACH land surface model'
 model_namelist_filename = "${lnd_namelist}"

/
&master_time_control_nml
 calendar             = "$calendar"
 restartTimeIntval    = "$restart_interval"
 checkpointTimeIntval = "$checkpoint_interval"
 experimentStartDate  = $start_date
 experimentStopDate   = $end_date
/
&time_nml
 calendar = $calendar_type
 is_relative_time = .FALSE.
/
EOF

#--------------------------------------------------------------------------------------------------

# (3) Define the model configuration

# atmospheric dynamics and physics
# --------------------------------
cat > ${atmo_namelist} << EOF
!
&parallel_nml
 nproma           = ${nproma}
 num_io_procs                =                          0         ! number of I/O processors
 num_restart_procs           =                          0         ! number of restart processors
/
&grid_nml
 dynamics_grid_filename      =  ${atmo_dyn_grids}
/
&run_nml
 num_lev          = 45          ! number of full levels
 modelTimeStep    = ${dtime}
 ltestcase        = .FALSE.     ! run testcase
 ldynamics        = .TRUE.      ! dynamics
 ltransport       = .TRUE.      ! transport
 ntracer          = 3           ! number of tracers; 3: hus, clw, cli; 4: hus, clw, cli, o3
 iforcing         = 2           ! 0: none, 1: HS, 2: ECHAM, 3: NWP
 output           = 'nml'
 msg_level        = 5           ! level of details report during integration 
 restart_filename = "${EXP}_restart_atm_<rsttime>.nc"
 activate_sync_timers = .TRUE.
/
&extpar_nml
 itopo            = 1           ! 1: read topography from the grid file
 l_emiss          = .FALSE.
/
&initicon_nml
 init_mode        = 2           ! 2: initialize from IFS analysis
 ifs2icon_filename= ${ifsfile}! name of file with IFS initial conditions (link file into working directory!)
/
&io_nml
 output_nml_dict  = "${dict_file}"
 netcdf_dict      = "${dict_file}"
/
&nonhydrostatic_nml
 ndyn_substeps    = 5           ! dtime/dt_dyn
 damp_height      = 50000.      ! [m]
 rayleigh_coeff   = 10.    	! default 0.1
 vwind_offctr     = 0.2
 divdamp_fac      = 0.004
 !htop_moist_proc  = 75000.      ! [m] Height above which moist physics and advection of cloud and precipitation variables are turned off; def-val = 22500.0
/
&interpol_nml
 rbf_scale_mode_ll = 1
/
&sleve_nml
 min_lay_thckn    = 40.         ! [m]
 top_height       = 72226.      ! [m]
 stretch_fac      = 0.949
 decay_scale_1    = 4000.       ! [m]
 decay_scale_2    = 2500.       ! [m]
 decay_exp        = 1.2
 flat_height      = 16000.      ! [m]
/
&diffusion_nml
 hdiff_smag_fac   = ${init_diff_fac}       ! scaling factor for smagorinsky diffusion; def-val = 0.015
 hdiff_efdt_ratio = 36.0        ! ratio of e-folding time to time step (or 2* time step when using a 3 time level time stepping scheme) (for triangular NH model, values above 30 are recommended when using hdiff_order=5)
 hdiff_w_efdt_ratio = 15.0      ! ratio of e-folding time to time step for diffusion on vertical wind speed
 hdiff_min_efdt_ratio = 1.0     ! minimum value of hdiff_efdt_ratio (for upper sponge layer); def-val = 1.0
 hdiff_tv_ratio = 1.0           ! Ratio of diffusion coefficients for temperature and normal wind: T : vn
/
&transport_nml
!                   hus,clw,cli
 ivadv_tracer     =   3,  3,  3
 itype_hlimit     =   3,  4,  4
 ihadv_tracer     =  52,  2,  2
/
&mpi_phy_nml
!
! domain 1
! --------
!
! atmospheric phyiscs (""=never)
 mpi_phy_config(1)%dt_rad = "PT2H"
 mpi_phy_config(1)%dt_vdf = $dtime
 mpi_phy_config(1)%dt_cnv = $dtime
 mpi_phy_config(1)%dt_cld = $dtime
 mpi_phy_config(1)%dt_gwd = $dtime
 mpi_phy_config(1)%dt_sso = $dtime

! atmospheric chemistry (""=never)
 mpi_phy_config(1)%dt_mox = ""
 mpi_phy_config(1)%dt_car = ""
 mpi_phy_config(1)%dt_art = ""
!
! seaice on mixed-layer ocean
 mpi_phy_config(1)%dt_ice = $dtime
!
! surface (.TRUE. or .FALSE.)
 mpi_phy_config(1)%ljsb  = ${ljsbach}
 mpi_phy_config(1)%lamip = .FALSE.
 mpi_phy_config(1)%lice  = .TRUE.
 mpi_phy_config(1)%lmlo  = .TRUE.
 mpi_phy_config(1)%llake  = ${llake}
!
/
&mpi_sso_nml
/
&radiation_nml
 irad_h2o         = 1           ! 1: prognostic vapor, liquid and ice
 irad_co2         = 2           ! 4: from greenhouse gas scenario
 vmr_co2          = 10000e-6
 irad_ch4         = 0           ! 4: from greenhouse gas scenario
 irad_n2o         = 0           ! 4: from greenhouse gas scenario
 irad_o3          = 4           ! 1: prognostic ozone; 8: prescribed transient monthly mean ozone
 irad_o2          = 2           ! 2: horizontally and vertically constant
 irad_cfc11       = 0           ! 4: from greenhouse gas scenario
 irad_cfc12       = 0           ! 4: from greenhouse gas scenario
 irad_aero        = 0          ! 0: no aerosol
                                !13: only Kinnes tropospheric aerosols
                                !14: only Stenchikovs volcanic aerosols
                                !15: Kinne aerosol optics for troposphere
                                !   +Stenchikov aerosol optics for stratosphere
                                !18: Kinne background aerosols of 1865 (natural
                                !    background + Stenchikovs volc. aerosols
                                !    + simple plumes (anthropogenic)
 ighg             = 0           ! 1: transient well mixed greenhouse gas concentrations
 isolrad          = 2           ! 1: transient solar irradiance (at 1 AE)
 scale_ssi_preind = 0.9442      ! requires isolrad = 2; scales ssi by specified value; default = 1
 albsnow_warm     = 0.66
 albsnow_cold     = 0.79
 albice_warm      = 0.38
 albice_cold      = 0.45
/
&psrad_nml
 rad_perm         = 1           ! Integer for perturbing random number seeds
/
&psrad_orbit_nml
 cecc = 0
 cobld = 23.5
 l_orbvsop87 = .FALSE.
/
&echam_conv_nml
/
&echam_cloud_nml
 csecfrl = 5e-5 ! [kgm^-3], default = 5e-6
/
&gw_hines_nml
/
&sea_ice_nml
 use_no_flux_gradients = .FALSE.
 i_ice_therm  = 1    ! 1=0L-Semtner; 2=3L-Winton
/
&echam_seaice_mlo_nml
 max_seaice_thickness = 9999.0 ! [m]
 hmin = 0.05                ! [m]
/
EOF

# land surface and soil
# ---------------------
cat > ${lnd_namelist} << EOF
&jsb_model_nml
  usecase         = "${jsbach_usecase}"
  fract_filename  = "bc_land_frac.nc"
  l_compat401     = .TRUE.              ! TRUE: overwrites some of the settings below
/
&jsb_seb_nml
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_rad_nml
  use_alb_veg_simple = .TRUE.           ! Use TRUE for jsbach_lite, FALSE for jsbach_pfts
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_turb_nml
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_sse_nml
  l_heat_cap_map  = .FALSE.
  l_heat_cond_map = .FALSE.
  l_heat_cap_dyn  = .TRUE.
  l_heat_cond_dyn = .TRUE.
  l_snow          = .TRUE.
  l_dynsnow       = .TRUE.
  l_freeze        = .FALSE.
  l_supercool     = .FALSE.
  bc_filename     = 'bc_land_soil.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_hydro_nml
  bc_filename     = 'bc_land_soil.nc'
  ic_filename     = 'ic_land_soil.nc'
  bc_sso_filename = 'bc_land_sso.nc'
/
&jsb_assimi_nml
  active          = .FALSE.             ! Use FALSE for jsbach_lite, TRUE for jsbach_pfts
/
&jsb_pheno_nml
  scheme          = 'climatology'       ! scheme = logrop / climatology; use climatology for jsbach_lite
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
EOF
if [[ ${jsbach_with_hd} = yes ]]; then
  cat >> ${lnd_namelist} << EOF
&jsb_hd_nml
  active               = .TRUE.
  routing_scheme       = 'full'
  bc_filename          = 'bc_land_hd.nc'
  diag_water_budget    = .TRUE.
  debug_hd             = .FALSE.
  enforce_water_budget = .TRUE.         ! True: stop in case of water conservation problem
/
EOF
fi


output_atm_2d=yes
#
if [[ "$output_atm_2d" == "yes" ]]; then
  #
  cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXP}_atm_2d"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = 5
 mode             = 1        ! 1=forecast mode, relative time axis
 taxis_tunit      = 9        ! 9=number of days since start
 remap            = 0
 operation        = 'mean'
 output_grid      = .TRUE.
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last     = .FALSE.
 ml_varlist       = 'ps'      , 'psl'     ,
                    'rsdt'    ,
                    'rsut'    , 'rsutcs'  , 'rlut'    , 'rlutcs'  ,
                    'rsds'    , 'rsdscs'  , 'rlds'    , 'rldscs'  ,
                    'rsus'    , 'rsuscs'  , 'rlus'    ,
                    'ts'      ,
                    'sic'     , 'sit'     , 'sic_icecl', 'qbot_icecl', 'qtop_icecl', 'ts_icecl', 't1_icecl', 't2_icecl', 'hs_icecl', 'fluxres_w_icecl', 'fluxres_i_icecl',
                    'albedo'  ,
                    'clt'     ,
                    'prlr'    , 'prls'    , 'prcr'    , 'prcs'    ,
                    'pr'      , 'prw'     , 'cllvi'   , 'clivi'   ,
                    'hfls'    , 'hfss'    , 'evspsbl' ,
                    'tauu'    , 'tauv'    ,
                    'tauu_sso', 'tauv_sso', 'diss_sso',
                    'sfcwind' , 'uas'     , 'vas'     ,
                    'tas'     , 'dew2'    ,

/
EOF
fi



output_atm_3d=yes
#
if [[ "$output_atm_3d" == "yes" ]]; then
  #
  cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXP}_atm_3d"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = 5
 taxis_tunit       = 9
 mode             = 1
 remap            = 0
 operation        = 'mean'
 output_grid      = .TRUE.
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last     = .FALSE.
 ml_varlist       = 'pfull'   , 'zg'      , 'rho' ,
                    'clw'     , 'cli'     ,
                    'hus'     , 'cl'      ,
                    'ta'      ,
                    'ua'      , 'va'      , 'wap'     ,
/
EOF
fi





## setup for status check & restart
final_status_file=${EXPDIR}/${EXP}.final_status

## Copy icon executable to working directory
#cp -p $ICONFOLDER/bin/icon ./icon.exe
cp -p $ICONFOLDER/build/x86_64-unknown-linux-gnu/bin/icon ./icon.exe
##

## adjust modulepath (see https://wiki.vsc.ac.at/doku.php?id=doku:spack-transition)
export MODULEPATH=/opt/sw/vsc4/VSC/Modules/TUWien:/opt/sw/vsc4/VSC/Modules/Intel/oneAPI:/opt/sw/vsc4/VSC/Modules/Parallel-Environment:/opt/sw/vsc4/VSC/Modules/Libraries:/opt/sw/vsc4/VSC/Modules/Compiler:/opt/sw/vsc4/VSC/Modules/Debugging-and-Profiling:/opt/sw/vsc4/VSC/Modules/Applications:/opt/sw/vsc4/VSC/Modules/p71545:/opt/sw/vsc4/VSC/Modules/p71782::/opt/sw/spack-0.19.0/var/spack/environments/skylake/modules/linux-almalinux8-x86_64:/opt/sw/spack-0.19.0/var/spack/environments/skylake/modules/linux-almalinux8-skylake
export LD_LIBRARY_PATH=/opt/sw/vsc4/VSC/x86_64/generic/arm/20.1_FORGE/lib/64:/opt/sw/vsc4/VSC/x86_64/generic/arm/20.1_FORGE/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/eccodes-2.25.0-hu7dgod7gf74ga4g3nsmcmpuocs6exiw/lib64:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/eccodes-2.25.0-hu7dgod7gf74ga4g3nsmcmpuocs6exiw/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/expat-2.4.8-xmo5dgbu5wtzbbbkvlrv4swwwfug44le/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/gdbm-1.19-jy4nm5ykjxpepom3ipbkze3zbyokjft3/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/gettext-0.21-pwxz72x7sx6qkqhbj4k3ubxxme26j3jc/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libbsd-0.11.5-cnqog4qv6nhpc5bvvsmcizf76cxr7jyd/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libffi-3.4.2-r3phrdc7ytbzn3leleegmrcr76cpx2ky/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libmd-1.0.4-zaniib3sfp7klwc5bmeqkglijauckuhr/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libpng-1.6.37-nkdaweppa2jmfn4ppg5nek6f4xxmazhd/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/openjpeg-2.3.1-qidbr475aivuv3j54clna4yif5ppnux4/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/py-numpy-1.16.6-o5kmt5ebnburugxciqwn7vws6kpll273/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/py-setuptools-44.1.1-xz4fgahr6psfstqpmh6lpv4rzumxiywg/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/sqlite-3.39.2-xf4wugvtkqpwzp2pcn57qreu3jdrryqz/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/readline-8.1.2-ufyd7l5fy7xsgxkgo324snjk2ltdflxz/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/ncurses-6.3-e42b4shzw4mv5dqaj72l5ji4l4qmmyym/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/bzip2-1.0.8-jv3bhggqmwgwhoyf4ptwwzyy37toaux6/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/util-linux-uuid-2.37.4-eic4im5vhjipymwciuywf63ifzwugjbi/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/zstd-1.5.2-rqo23z7mor7xn22cd45agw5g2hbvtuvj/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libxml2-2.10.1-3htfdmnkdmy5etoxzynnvx22vhfxr2mj/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/xz-5.2.5-7mgkxyje4sp5zdyq3z4dogzup4ulmrm5/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/libiconv-1.16-4rpaj4ypa7ib7s5z7tiqqmu7tfuai7gj/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-mkl-2022.1.0-cvhktedeellvvlgsjf3pap7vpx6f55z5/mkl/2022.1.0/lib/intel64:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-tbb-2021.6.0-xb42jplakefi5zt667wrhottjpksgd7d/tbb/2021.6.0/env/../lib/intel64/gcc4.8:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/netcdf-fortran-4.6.0-pnaropyoft7hicu7bfsugqa2aqcsggxj/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/netcdf-c-4.8.1-hmrqrz22oi6fn4uorchs36xxm5ruffhr/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-skylake/intel-2021.5.0/hdf5-1.12.2-loke5pdbheud7c2wtvcefl6ao42ui7ia/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/zlib-1.2.12-pctnhmb36u364evebp7adp4qdmziy3mj/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/pkgconf-1.8.0-bkuyrr7xuzspbxljk66eussj4inp5oeh/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-mpi-2021.6.0-wpt4y32prmkcjdsos57rj6chrpa2yt2g/mpi/2021.6.0//libfabric/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-mpi-2021.6.0-wpt4y32prmkcjdsos57rj6chrpa2yt2g/mpi/2021.6.0//lib/release:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-mpi-2021.6.0-wpt4y32prmkcjdsos57rj6chrpa2yt2g/mpi/2021.6.0//lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/gcc-8.5.0/intel-oneapi-compilers-2022.1.0-kiyqwf7md4zpdhweoetajmd5hu2yme65/compiler/2022.1.0/linux/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/gcc-8.5.0/intel-oneapi-compilers-2022.1.0-kiyqwf7md4zpdhweoetajmd5hu2yme65/compiler/2022.1.0/linux/lib/x64:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/gcc-8.5.0/intel-oneapi-compilers-2022.1.0-kiyqwf7md4zpdhweoetajmd5hu2yme65/compiler/2022.1.0/linux/lib/oclfpga/host/linux64/lib:/gpfs/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/gcc-8.5.0/intel-oneapi-compilers-2022.1.0-kiyqwf7md4zpdhweoetajmd5hu2yme65/compiler/2022.1.0/linux/compiler/lib/intel64_lin::/opt/sw/slurm/x86_64/alma8.5/22-05-2-1/lib:/opt/sw/slurm/x86_64/alma8.5/22-05-2-1/lib/slurm


## Start model
date
ulimit -s unlimited

source /usr/share/Modules/init/ksh
module purge
module load intel-oneapi-compilers/2022.1.0-gcc-8.5.0-kiyqwf7
module load intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
module load pkgconf/1.8.0-intel-2021.5.0-bkuyrr7
module load zlib/1.2.12-intel-2021.5.0-pctnhmb
module load hdf5/1.12.2-intel-2021.5.0-loke5pd
module load netcdf-c/4.8.1-intel-2021.5.0-hmrqrz2
module load netcdf-fortran/4.6.0-intel-2021.5.0-pnaropy
module load intel-oneapi-mkl/2022.1.0-intel-2021.5.0-cvhkted --auto
module load libiconv/1.16-intel-2021.5.0-4rpaj4y
module load libxml2/2.10.1-intel-2021.5.0-3htfdmn --auto
module load eccodes/2.25.0-intel-2021.5.0-hu7dgod --auto


echo "loading arm"
module load arm/20.1_FORGE

ldd icon.exe

START="/opt/sw/spack-0.19.0/opt/spack/linux-almalinux8-x86_64/intel-2021.5.0/intel-oneapi-mpi-2021.6.0-wpt4y32prmkcjdsos57rj6chrpa2yt2g/mpi/2021.6.0/bin/mpiexec -n $mpi_total_procs"
MODEL=${EXPDIR}/icon.exe

rm -f finish.status

${START} ${MODEL}

if [ -r finish.status ] ; then  # if finish.status exists, continue
  echo "finish.status exists, continue"
else # if it not exists, abort (or change diffusion parameter!)
  echo "CRASH" > finish.status
fi

#-----------------------------------------------------------------------------
finish_status=`cat finish.status`
echo $finish_status

#-----------------------------------------------------------------------------
namelist_list=""
#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ]; then # restart simulation
  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/exp.${EXP}.run"
  echo 'this_script: ' "$this_script"
  # note that if ${restartSemaphoreFilename} does not exist yet, then touch will create it
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  sbatch exp.${EXP}.run
elif  [ $finish_status = "CRASH" ]; then # restart simulation after crash
  echo "model crashed, changing diffusion parameter..."
  echo "old diffusion parameter: ${init_diff_fac}" 
  if (( $(echo "$init_diff_fac == 0.015" |bc ) )); then
    echo 0.000001 > hdiff_smag_fac_offset
    echo "new diffusion parameter: 0.015001"
    log="- $(date '+%d-%m-%Y %H:%M:%S') crash, autorestart with hdiff_smag_fac=0.015001"
  else
    if [ -r hdiff_smag_fac_offset ]; then
      rm -f hdiff_smag_fac_offset
    fi
    echo "new diffusion parameter: 0.015"
    log="- $(date '+%d-%m-%Y %H:%M:%S') crash, autorestart with hdiff_smag_fac=0.015"
  fi


  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/exp.${EXP}.run"
  echo 'this_script: ' "$this_script"
  # note that if ${restartSemaphoreFilename} does not exist yet, then touch will create it
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  sbatch exp.${EXP}.run
  echo -e ${log} >> README.md
  # abort the script, so SLURM will sent a mail
  exit 100
elif  [ $finish_status = "OK" ]; then # end simulation
  [[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi

#-----------------------------------------------------------------------------

cd ${RUNSCRIPTDIR}

#-----------------------------------------------------------------------------
#

echo "============================"
echo "Script run successfully: ${finish_status}"
echo "============================"
#-----------------------------------------------------------------------------

