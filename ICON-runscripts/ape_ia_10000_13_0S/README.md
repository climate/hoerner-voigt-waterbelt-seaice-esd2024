# ape_ia_10000_13_0S
ICON-A run. Same settings as ape_ia_8000_13_0S, except for CO2.  Start from restart file ape_ia_6000_90_0S_restart_atm_00200101T000000Z.nc (sic=0.767578125000124). CO2 9000ppmv, time step =6min, damping=10.
This simulation is expected to melt. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 21.06.23 11:30 - simulation start at year 200, end year 400
- 21-06-2023 11:40:45 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:43:38 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:44:17 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:44:53 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:45:29 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:46:07 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:46:43 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:47:22 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:47:56 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:48:31 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:49:04 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:49:39 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:50:14 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:50:49 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:51:27 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:52:03 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:52:39 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:53:14 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:53:50 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:54:27 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:55:04 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:55:39 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:56:14 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:56:47 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:57:22 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:57:58 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:58:30 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 00:59:06 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 00:59:42 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:00:17 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:00:49 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:01:24 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:01:57 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:02:32 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:03:05 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:03:40 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:04:15 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:04:50 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:05:26 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:06:03 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:06:38 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:07:16 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:07:52 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:08:28 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:09:03 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:09:40 crash, autorestart with hdiff_smag_fac=0.015
- 26-06-2023 01:10:15 crash, autorestart with hdiff_smag_fac=0.015001
- 26-06-2023 01:10:52 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 03:00 continue until 350-01-01
- 08-07-2023 03:00 timeout at 334-01-17, continue
- 13-07-2023 00:00 finish at 350-01-01
