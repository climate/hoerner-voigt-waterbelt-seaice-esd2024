# ape_ia_8000_13_0Sto3W
partner simulation to ape_ia_8000_13_0Sto3W.

Simulation restarted from 0L-Semtner in Waterbelt state, continued with 0L-Semtner sea-ice model (just with higher output to compare to simulation with 3L-Winton model). Same settings as ape_ia_8000_13_0S only with higher output. Restart file is adapted to include a approximation of initial ice-internal temperatures

Start from restart file ape_ia_8000_13_0S_restart_atm_04000101T000000Z_icetemps.nc (created on jet by icetemps.ipynb). CO2 8000ppmv, time step =6min, damping=10.
This simulation is expected to stay in a waterbelt state. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 03-05-2022 14:00 - simulation start at 400-01-01, end date 420-01-01
