# ape_ia_6500_90_0S
NOT USED! timestep was reduced too early, that's why it fell into a Snowball. Instead ape_ia_6500_90_0S_cltlim_dtime10 is used.

Second production run using ICON-A. Same settings as ape_ia_6000_90_0S, esxept for CO2.  This simulation is expected to reach a stable ice-edge (compare to Braun et al. 2022, simulation exp.mlo_aqua_6500ppmv_BFP.run. As in that simulation, csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
12 hours time with 10 years restart interval


## explog
20.03.23 17:45 - simulation start at year 1, end year 100
- 20-03-2023 19:41:53 crash, autorestart with hdiff_smag_fac=0.015001
- 21-03-2023 15:06:02 crash, autorestart with hdiff_smag_fac=0.015
- 21-03-2023 18:21:22 crash, autorestart with hdiff_smag_fac=0.015001, decrease timestep to 8min (in the first year)
- 22-03-2023 04:11:59 crash, autorestart with hdiff_smag_fac=0.015
- 22-03-2023 10:28:20 crash, autorestart with hdiff_smag_fac=0.015001
- 23-03-2023 11:30 timeout at 16-04-25, increase duration to 15h
- 03-04-2023 11:00 finish at 100-01-01
- 12-04-2023 12:00 continue until 200-01-01, with timestep=6min, restart every 20years
- 14-04-2023 10:45 abort at 146-11-26
