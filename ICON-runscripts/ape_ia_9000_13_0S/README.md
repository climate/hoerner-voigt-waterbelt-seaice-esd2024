# ape_ia_9000_13_0S
5th production run using ICON-A. Same settings as ape_ia_8000_13_0S, except for CO2.  Start from restart file ape_ia_6000_90_0S_restart_atm_00200101T000000Z.nc (sic=0.767578125000124). CO2 9000ppmv, time step =6min, damping=10.
This simulation is expected to melt. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 06.04.23 14:00 - simulation start at year 200, end year 300
- 12.04.23 00:30 - finish at 300-01-01
- 13-04-23 12:00 continue until 400-01-01
- 15-04-2023 22:49:14 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:51:18 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:53:15 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:55:16 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:57:18 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:59:01 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:01:17 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:03:03 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:05:18 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:07:02 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:09:18 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:11:18 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:13:04 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:15:22 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:17:19 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:19:19 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:21:06 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:23:05 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:25:05 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:27:05 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:29:23 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:31:24 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:33:21 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:35:05 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:37:23 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:39:07 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:41:07 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:43:25 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:45:24 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:47:08 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:49:11 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:51:25 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:53:25 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:55:26 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:57:26 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:59:26 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:01:09 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:03:25 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:05:26 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:07:26 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:09:26 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:11:26 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:13:28 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:15:27 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:17:16 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:19:27 crash, autorestart with hdiff_smag_fac=0.015, disk limit at 381-07-05
- 16-04-2023 11:20 continue until 400-01
- 19-04-2023 19:00 TIMEOUT at 394-06-09, restart
- 21-04-2023 08:00 finish at 400-01-01

