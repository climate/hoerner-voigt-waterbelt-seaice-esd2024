# ape_ia_9000_90_3W
Winton run with ICON-A. I use the same initial conditions and boundary conditions as for the Semtner runs. Damping parameter starts with 0.1 and is increased after crashes. Time step is 10 min. csecfrl = 5e-5. Auto restart is enabled from the start. 


model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 22.06.23 18:00 - simulation start at year 1, end year 100
- 22-06-2023 18:21:41 crash, autorestart with hdiff_smag_fac=0.015001
- 22-06-2023 18:33:47 crash, autorestart with hdiff_smag_fac=0.015
- 22-06-2023 22:36:36 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 04:14:56 crash, autorestart with hdiff_smag_fac=0.015
- 23-06-2023 11:43:43 crash, autorestart with hdiff_smag_fac=0.015001
- 23-06-2023 15:19:31 crash, autorestart with hdiff_smag_fac=0.015
- 24-06-2023 02:41:13 crash, autorestart with hdiff_smag_fac=0.015001
- 24-06-2023 13:55:59 crash, autorestart with hdiff_smag_fac=0.015
- 24-06-2023 17:50:26 crash, autorestart with hdiff_smag_fac=0.015001
- 24-06-2023 19:58:33 crash, autorestart with hdiff_smag_fac=0.015
- 25-06-2023 03:41:47 crash, autorestart with hdiff_smag_fac=0.015001
  25-06-2023 10:00 finish at year 100
