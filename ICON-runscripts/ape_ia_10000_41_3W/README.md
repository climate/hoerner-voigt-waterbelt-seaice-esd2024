#ape_ia_10000_41_3W
Winton run with ICON-A, branch off from ape_ia_8000_90_3W, year 190. All settings the same as there (except CO2...)  Damping parameter is 10. Time step is 6 min. csecfrl = 5e-5. Auto restart is enabled from the start. 
restart file ape_ia_8000_90_3W_restart_atm_01900101T000000Z.nc

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 31-07-2023 15:30 simulation start, end year 240
