# ape_ia_5000_13_0S
6th production run using ICON-A. Same settings as ape_ia_8000_13_0S, except for CO2.  Start from restart file ape_ia_6000_90_0S_restart_atm_00200101T000000Z.nc (sic=0.767578125000124). CO2 5000ppmv, time step =6min, damping=10.
This simulation is expected to fall into a Snowball. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 06.04.23 14:00 - simulation start at year 200, end year 300
- 12.04.23 01:00 - finish at 300-01-01
- 12-04-23 12:00 continue until 400-01-01
- 15-04-23 07:00 abort at 352-12-17 due to time limit? (probably because of full disk quota)
- 17-04-23 15:30 continue
- 19-04-23 10:00 finish at 400-01-01
