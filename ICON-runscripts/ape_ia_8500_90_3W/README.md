# ape_ia_8500_90_3W
Winton run with ICON-A. I use the same initial conditions and boundary conditions as for the Semtner runs. Damping parameter starts with 0.1 and is increased after crashes. Time step is 10 min. csecfrl = 5e-5. Auto restart is enabled from the start. 


model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 06.07.23 16:00 - simulation start at year 1, end year 100
- 07-07-2023 04:37:41 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 04:40:53 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 04:44:37 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 04:47:02 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 04:50:45 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 04:53:08 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 04:56:51 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 04:59:15 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:03:02 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:05:25 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:09:16 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:11:39 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:15:28 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:17:55 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:21:46 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:24:12 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:28:06 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:30:32 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:34:23 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:36:49 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:40:41 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:43:06 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:46:59 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:49:25 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:53:18 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 05:55:43 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 05:59:35 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:02:02 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:05:55 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:08:19 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:12:12 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:14:36 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:18:28 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:20:55 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:24:45 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:27:14 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:31:04 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:33:31 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:37:17 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:39:43 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:43:36 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:46:01 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:49:49 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:52:16 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 06:56:08 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 06:58:30 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:02:21 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:04:47 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:08:36 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:11:01 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:14:50 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:17:16 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:21:09 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:23:37 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:27:30 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:30:00 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:33:56 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:36:24 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:40:19 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:42:49 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:46:44 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 07:49:15 crash, autorestart with hdiff_smag_fac=0.015
- 07-07-2023 07:53:05 crash, autorestart with hdiff_smag_fac=0.015001
- 07-07-2023 09:58:30 crash loop at 3-01-20, restart at 2-07-01 with hdiff_smag_fac=0.014998
- 09-07-2023 12:47:24 crash, autorestart with hdiff_smag_fac=0.015
- 09-07-2023 20:13:26 crash, autorestart with hdiff_smag_fac=0.015001
- 10-07-2023 05:49:49 crash, autorestart with hdiff_smag_fac=0.015
- 10-07-2023 15:03:55 crash, autorestart with hdiff_smag_fac=0.015001
- 10-07-2023 16:38:28 crash, autorestart with hdiff_smag_fac=0.015
- 10-07-2023 20:03:43 crash, autorestart with hdiff_smag_fac=0.015001
- 10-07-2023 21:28:00 crash, autorestart with hdiff_smag_fac=0.015
- 10-07-2023 23:35:07 crash, autorestart with hdiff_smag_fac=0.015001
- 11-07-2023 07:20:35 crash, autorestart with hdiff_smag_fac=0.015
- 11-07-2023 16:35:58 crash, autorestart with hdiff_smag_fac=0.015001
- 11-07-2023 23:00 finish on 100-01-01
