# ape_ia_8000_13_0S
4th production run using ICON-A. Same settings as ape_ia_6000_90_0S, except for CO2 and starting date.  Start from restart file ape_ia_6000_90_0S_restart_atm_00200101T000000Z.nc (sic=0.767578125000124). CO2 8000ppmv, time step =6min, damping=10.
This simulation is expected to stay in a waterbelt state. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 06.04.23 14:00 - simulation start at year 200, end year 300
- 11.04.23 23:00 - finish at 300-01-01
- 12-04-23 12:00 continue until 400-01-01
- 15-04-2023 22:48:25 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:49:01 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:51:00 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:53:17 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:55:16 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 22:57:03 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 22:59:16 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:01:18 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:03:17 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:05:18 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:07:19 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:09:05 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:11:18 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:13:22 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:15:20 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:17:06 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:19:22 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:21:19 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:23:19 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:25:20 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:27:20 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:29:22 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:31:05 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:33:23 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:35:22 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:37:23 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:39:22 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:41:23 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:43:07 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:45:08 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:47:25 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:49:25 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:51:24 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:53:10 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:55:09 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 23:57:25 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 23:59:09 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:01:26 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:03:25 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:05:10 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:07:15 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:09:27 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:11:12 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:13:28 crash, autorestart with hdiff_smag_fac=0.015
- 16-04-2023 00:15:13 crash, autorestart with hdiff_smag_fac=0.015001
- 16-04-2023 00:17:28 crash, autorestart with hdiff_smag_fac=0.015, disk limit at 386-02-12
- 18-04-2023 11:20 continue until 400-01
- 19-04-2023 06:00 finish
- 03-05-2023 14:37:44 crash, autorestart with hdiff_smag_fac=0.015001
