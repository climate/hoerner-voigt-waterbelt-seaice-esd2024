# ape_ia_10000_13_0Sto3W
Partner simulation to ape_ia_1000_13_0Sto0S

Simulation restarted from 0L-Semtner in Waterbelt state, with 3L-Winton sea-ice model. Same settings as ape_ia_10000_13_0S, except for sea-ice mode. Restart file is adapted to include a approximation of initial ice-internal temperatures
Start from restart file ape_ia_10000_13_0S_restart_atm_03500101T000000Z_icetemps.nc (created on jet by icetemps.ipynb). CO2 10000ppmv, time step =6min, damping=10.
This simulation is expected to freeze to a Snowball due to the ice scheme. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 17-07-2022 15:00 - simulation start at 350-01-01, end date 360-01-01
- 17-07-2023 20:21:35 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 22:08:01 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 22:22:08 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 22:50:10 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 23:08:16 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 23:23:18 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 23:32:18 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 23:44:26 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:11:09 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:14:52 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:18:25 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:20:22 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:22:21 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:24:21 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:26:22 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:28:22 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:30:21 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:32:23 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:34:22 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:36:46 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:38:22 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:42:08 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:42:23 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:44:23 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:46:24 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:48:28 crash, autorestart with hdiff_smag_fac=0.015
