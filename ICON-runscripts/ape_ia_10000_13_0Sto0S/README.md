# ape_ia_10000_13_0Sto3W
partner simulation to ape_ia_10000_13_0Sto3W.

Simulation restarted from 0L-Semtner in Waterbelt state, continued with 0L-Semtner sea-ice model (just with higher output to compare to simulation with 3L-Winton model). Same settings as ape_ia_10000_13_0S only with higher output. Restart file is adapted to include a approximation of initial ice-internal temperatures

Start from restart file ape_ia_10000_13_0S_restart_atm_03500101T000000Z_icetemps.nc (created on jet by icetemps.ipynb). CO2 8000ppmv, time step =6min, damping=10.
This simulation is expected to stay in a waterbelt state. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 17-07-2022 15:00 - simulation start at 350-01-01, end date 360-01-01
- 17-07-2023 21:23:48 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 22:20:05 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 22:38:19 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 22:56:49 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 23:12:15 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 23:23:14 crash, autorestart with hdiff_smag_fac=0.015
- 17-07-2023 23:32:18 crash, autorestart with hdiff_smag_fac=0.015001
- 17-07-2023 23:46:18 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:10:21 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:13:47 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:16:20 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:18:25 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:22:21 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:24:21 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:26:26 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:28:22 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:30:21 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:32:22 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:34:26 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:36:52 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:38:22 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:40:26 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:44:22 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:46:23 crash, autorestart with hdiff_smag_fac=0.015
- 18-07-2023 00:48:24 crash, autorestart with hdiff_smag_fac=0.015001
- 18-07-2023 00:50:23 crash, autorestart with hdiff_smag_fac=0.015
