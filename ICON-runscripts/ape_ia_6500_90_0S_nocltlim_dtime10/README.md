# ape_ia_6500_90_0S_nocltlim_dtime10

Same as ape_ia_6500_90_0S_nocltlim, but whith initial time step =10s to test for sensitivity 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 14.04.23 17:30 - simulation start at year 1, end year 40
- 13-04-2023 19:17:53 crash, autorestart with hdiff_smag_fac=0.015001
- 13-04-2023 19:36:17 crash, autorestart with hdiff_smag_fac=0.015
- 13-04-2023 19:56:06 crash, autorestart with hdiff_smag_fac=0.015001
- 13-04-2023 20:16:29 crash, autorestart with hdiff_smag_fac=0.015
- 13-04-2023 20:20:13 crash, autorestart with hdiff_smag_fac=0.015001
- 13-04-2023 20:38:36 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 03:51:26 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 04:45:30 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 04:53:48 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 05:33:26 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 05:42:01 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 06:21:45 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 06:30:13 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 07:19:48 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 07:26:09 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 07:44:34 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 07:50:14 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 08:08:43 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 08:14:26 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 08:32:48 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 08:38:23 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 08:56:43 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 09:00:36 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 09:18:58 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 09:22:36 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 09:40:58 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 09:46:39 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 10:05:11 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 10:15 change rayleigh damping to 0.1, restart from initial conditions 
- 14-04-2023 12:27:22 crash, autorestart with hdiff_smag_fac=0.015001
- 14-04-2023 12:29:45 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 14:15:20 crash, manual restart with hdiff_smag_fac=0.014998
- 14-04-2023 23:17:40 crash, autorestart with hdiff_smag_fac=0.015
- 14-04-2023 23:19:30 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 02:03:01 crash, autorestart with hdiff_smag_fac=0.015
- 15-04-2023 09:52:09 crash, autorestart with hdiff_smag_fac=0.015001
- 15-04-2023 16:40 finish at 40-01-01
