# ape_ia_7000_62_0S
NOT USED! Timestep was reduced too early.

Third production run using ICON-A. Same settings as ape_ia_6500_90_0S, except for CO2 and starting date. As ape_ia_6500_90_0S didn't reach a stable high-latitude ice-edge, this runs increases C02. Start from restart file ape_ia_6500_90_0S_restart_atm_00100101T000000Z.nc (sic=0.11972656250004) to reduce spin-up. CO2 7000ppmv, time step =8min, damping=10.
This simulation is expected to reach a stable ice-edge. csecfrl = 5e-5. Auto restart is enabled from the start. 

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
-03.04.23 13:30 - simulation start at year 10, end year 100
- 03-04-2023 18:47:44 crash, autorestart with hdiff_smag_fac=0.015001
- 04-04-2023 00:14:32 crash, autorestart with hdiff_smag_fac=0.015
- 04-04-2023 05:07:27 crash, autorestart with hdiff_smag_fac=0.015001
- 04-04-2023 15:03:01 crash, autorestart with hdiff_smag_fac=0.015
- 06-04-2023 14:00 stop at 97-01-01, continue until 150
- 11-04-2023 11:00 finish at 150, continue until 170
- 12-04-2023 01:00 finish at 170-01-01
- 12-04-2023 12:00 continue until 200-01-01
- 13-04-2023 08:34:04 crash, autorestart with hdiff_smag_fac=0.015001
- 13-04-2023 14:30 finish at 200-01-01
