# ape_ia_6500_90_0S_nocltlim

ICON-A run to test if htop_moist_proc (upper limit of moist processes) has an impact on the climate. This run has the same settings as ape_ia_6500_90_0S, except: htop_moist_proc=75000 (instead of 25000, de facto this means no upper limit) and dtime=8min from the start (as ape_ia_6500_90_0S was so unstable that timestep was decreased to 8min in the first years)

model version: [commit 01fa4282 in icon-aes-dkrz/icon-aes-mlo-jh](https://gitlab.phaidra.org/hoernerj21/icon-aes-dkrz/-/commit/01fa42823070f664f806b7d54c1d0ffef3e308df)

5 nodes
account p71767 (project nodes)
24 hours time with 20 years restart interval


## explog
- 14.04.23 16:30 - simulation start at year 1, end year 40
- 14.04.23 22:00 - finish at 40-01-01 
